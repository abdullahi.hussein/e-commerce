<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=> 'admin', 'middleware'=>['admin:admin']], function(){
	Route::get('/login', [AdminController::class, 'loginForm']);
	Route::post('/login',[AdminController::class, 'store'])->name('admin.login');
});




Route::middleware(['auth:sanctum,admin', 'verified'])->get('/admin/dashboard', function () {
    return view('admin.index');
})->name('dashboard');

//admin

Route::get('/admin/logout', [AdminController::class, 'destroy'])->name('admin.logout');
Route::get('/admin/profile', [\App\Http\Controllers\Backend\AdminProfileController::class, 'AdminProfile'])->name('admin.profile');
Route::get('/admin/profile/edit', [\App\Http\Controllers\Backend\AdminProfileController::class, 'AdminProfileEdit'])->name('admin.profile.edit');
Route::post('/admin/profile/store', [\App\Http\Controllers\Backend\AdminProfileController::class, 'AdminProfileStore'])->name('admin.profile.store');
Route::get('/admin/password/change', [\App\Http\Controllers\Backend\AdminProfileController::class, 'AdminPasswordChange'])->name('admin.change.password');
Route::post('/admin/password/update', [\App\Http\Controllers\Backend\AdminProfileController::class, 'AdminUpdatePassword'])->name('update.change.password');




Route::middleware(['auth:sanctum,web', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/', [\App\Http\Controllers\IndexController::class, 'index']);


//brands
Route::prefix('brand')->group(function(){
    Route::get('/view', [\App\Http\Controllers\Backend\BrandController::class, 'BrandView'])->name('all.brands');
});

@extends('admin.admin_master')

@section('admin')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <section class="content">

        <div class="row">

            <div class="col-12">
                <div class="box">
                    <form method="post" action="{{ route('update.change.password') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="box-header">
                            <h4 class="box-title">Admin Password Change</h4>
                        </div>
                        <div class="box-body">

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Current Password<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input class="form-control" type="password" id="current_password" name="oldpassword"
                                                   ></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>New Password<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input class="form-control" id="password" type="password"  name="password"
                                            ></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Confirm Password<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input class="form-control" type="password" id="password_confirmation" name="password_confirmation"
                                            ></div>
                                    </div>
                                </div>
                            </div>

                            </div>
                            <button class="btn btn-rounded btn-primary btn-md mt-2 ml-5" type="submit">Update Profile</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>


    </section>

@endsection

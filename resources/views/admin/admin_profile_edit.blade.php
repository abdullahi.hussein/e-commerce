@extends('admin.admin_master')

@section('admin')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <section class="content">

        <div class="row">

            <div class="col-12">
                <div class="box">
                    <form method="post" action="{{ route('admin.profile.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="box-header">
                            <h4 class="box-title">Admin Profile Edit</h4>
                        </div>
                        <div class="box-body">

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Admin E-mail<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input class="form-control" type="text" name="name"
                                                   value="{{ $editData->name }}"></div>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Admin E-mail<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input class="form-control" type="email" name="email"
                                                   value="{{ $editData->email }}"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-form-label col-lg-2">Image</label>
                                        <div class="col-lg-12">
                                            <input type="file" name="profile_photo_path" class="form-control"
                                                   id="image">
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="widget-user">

                                            <img id="showImage" class="rounded-circle"
                                                 src="{{ (!empty($adminData->profile_photo_path)) ? url('upload/admin_images/'.$adminData->profile_photo_path) : url('upload/no_image.jpeg') }}"
                                                 alt="User Avatar" style="width:100px; height:100px">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button class="btn btn-rounded btn-primary btn-md mt-2 ml-5" type="submit">Update Profile</a>

                        </div>
                    </form>
            </div>
            </div>
        </div>


    </section>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#image').change(function (e) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            });
        });


    </script>

@endsection
